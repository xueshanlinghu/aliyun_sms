# 测试阿里云的短信发送接口

# 需要提前装好核心库：pip install aliyun-python-sdk-core

import aliyun_sms as sms
import uuid

__business_id = uuid.uuid1()

# 一个验证码发送的例子
params = '{"code":"456238"}'  #模板参数
print(str(sms.send_sms(__business_id, "你的手机号", "短信签名", "申请的短信模板编码", params), encoding = 'utf-8'))