#!/usr/bin/env python
#coding=utf-8

# 阿里云短信发送静态配置文件
# 作者：雪山凌狐
# 版本号：1.0
# 修改时间：20200102

# ACCESS_KEY_ID/ACCESS_KEY_SECRET 根据实际申请的账号信息进行替换
ACCESS_KEY_ID = "你申请的ACCESS_KEY_ID"
ACCESS_KEY_SECRET = "你申请的ACCESS_KEY_ID"
