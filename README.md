# 阿里云短信验证码接口

#### 介绍
自用的阿里云新版验证码接口整合python包，可简简单单开始使用。有详细的demo和注释。

#### 使用说明
1.先将所有代码下载到本地，安装好阿里云发送短信的核心库：

```py
pip install aliyun-python-sdk-core
```

2.在aliyun_sms/const.py中填入自己在阿里云添加的具有短信发送权限的RMA账号的： **ACCESS_KEY_ID**和**ACCESS_KEY_ID** ，具体可以参见阿里云的官方帮助文档

```py
ACCESS_KEY_ID = "你申请的ACCESS_KEY_ID"
ACCESS_KEY_SECRET = "你申请的ACCESS_KEY_ID"
```

3.修改aliyun_sms/sms_send.py的最后一行后，测试包运行正常。在此之前，你需要购买阿里云的短信服务，并获取到短信签名、短信发送模板等信息，这里的demo均是演示发送一个验证码短信的。

```py
print(str(send_sms(__business_id, "你的手机号", "短信签名", "申请的短信模板编码", params), encoding = 'utf-8'))
```

这里修改好你的手机号、短信签名、申请的短信模板编码即可测试。

4.包运行正常后，可以打开sendsms_test.py进行修改使用了。初次调试完成后，平时就可以模仿sendsms_test.py里面的写法整合到自己的python程序当中。

```py
print(str(sms.send_sms(__business_id, "你的手机号", "短信签名", "申请的短信模板编码", params), encoding = 'utf-8'))
```

跟包的测试一样，修改手机号、短信签名、申请的短信模板编码即可测试。


### ENJOY YOUR CODE !